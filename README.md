# Hello-express

This project contains REST API with following features:

- Welcome endpoint
- Multiply endpoint

## Getting started

First install the denpendencies

```sh
npm i
```

Start the REST API service:
`node app.js`

Or in the latest NodeJS version (>= 20):

`node --watch app.js`

In this initalization i have added isNan Errors to the return statement:

![screenshot from terminal](/assets/error.JPG "screenshot from terminal")